// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Contract is ERC721Enumerable, Ownable {
    using Strings for uint256;
    
    uint256 public constant MAX_TOKENS = 8000;
    uint256 public constant MAX_MINT = 20;
    uint256 public constant OWNER_CLAIM_LIMIT = 1;
    address public constant devAddress = 0x1234;
    
    string public baseURI;
    string public baseExtension = '.json';
    uint256 public price = 1 ether;
    uint256 public ownerClaimCount = 0;
    
    bool private isSaleActive = false;
    
    constructor(string memory _name, 
                string memory _symbol, 
                string memory _initBaseURI) ERC721(_name, _symbol) {
        setBaseURI(_initBaseURI);
    }
    
    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }
    
    function walletOfOwner(address _owner) external view returns (uint256[] memory) {
        uint256 tokenCount = balanceOf(_owner);
        uint256[] memory tokenIds = new uint256[](tokenCount);
        for (uint256 i = 0; i < tokenCount; i++) {
            tokenIds[i] = tokenOfOwnerByIndex(_owner, i);
        }
        return tokenIds;
    }
    
    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(
            tokenId <= totalSupply() && tokenId > 0,
            "URI query for nonexistant token"
        );
        string memory currBaseURI = _baseURI();
        return bytes(currBaseURI).length > 0
        ? string(abi.encodePacked(currBaseURI, tokenId.toString(), baseExtension))
        : "";
        
    }
    
    function mint(address _to, uint256 _amount) public payable {
        uint256 supply = totalSupply();
        require(isSaleActive, "Sale is not active");
        require(_amount > 0, "Must mint at least 1 token");
        require(_amount < MAX_MINT, "Exceeds mint limit");
        require(supply + _amount <= MAX_TOKENS, "Exceeds total supply");
        require(msg.value >= _amount * price, "not enough ETH for mint");
        
        for (uint256 i = 1; i <= _amount; i++) {
            _safeMint(_to, supply + 1);
        }
    }
    
    function toggleSale() public onlyOwner {
        isSaleActive = !isSaleActive;
    }
    
    function setPrice(uint256 _newPrice) public onlyOwner {
        price = _newPrice;
    }
    
    function ownerClaim(address _to, uint256 _amount) public onlyOwner {
        uint256 supply = totalSupply();
        require(_amount + ownerClaimCount <= OWNER_CLAIM_LIMIT, "Exceeds owner claim limit");
        require(supply + _amount <= MAX_TOKENS, "Exceeds total supply");
        
        for (uint256 i = 1; i <= _amount; i++) {
            _safeMint(_to, supply + i);
            ownerClaimCount += 1;
        }
    }
    
    function setBaseURI(string memory newURI) public onlyOwner {
        baseURI = newURI;
    }
    
    function setBaseExt(string memory newExt) external onlyOwner {
        baseExtension = newExt;
    }
    
    function withdraw() public payable onlyOwner {
        uint256 balance = address(this).balance;
        payable(msg.sender).transfer(balance);
    }
}