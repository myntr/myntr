from typing import Collection
import random
import sys
import os
import json
from dataclasses import dataclass
from pathlib import Path
from enum import EnumMeta
from config import *
from PIL import Image


full_path = os.path.realpath(__file__)
path, filename = os.path.split(full_path)

def getDnaFromStr(dnaStr):
    dna = []
    n = 2

    splitStr = [(dnaStr[i:i+n]) for i in range(0, len(dnaStr), n)]
    splitStr.pop() # to remove /n from list
    
    for dnaItem in splitStr:
        dna.append(int(dnaItem))

    return dna

def saveImage(imageFiles, editionNumber, dir):
    images = []

    for imageFile in imageFiles:
        image = Image.open(imageFile).convert('RGBA')
        images.append(image)
    
    # composite image, assume at least 2 images
    composite = Image.alpha_composite(images[0], images[1])
    for i in range(2, len(images)):
        composite = Image.alpha_composite(composite, images[i])

     #Convert to RGB
    rgb_im = composite.convert('RGB')
    file_name = str(editionNumber) + ".png"
    rgb_im.save(dir + PROJ_DIR + "output/" + file_name)



def generate():
    print("\nstarting nft generation...")
    dir = os.path.dirname(os.path.realpath(__file__))
    for machine in range(1, MACHINE_COUNT + 1):
        fileName = dir + PROJ_DIR + DNA_FILE + str(machine) + '.txt'
        traits = getTraitsFromFile()
        
        if os.path.isfile(fileName):
            print(f"generating nfts for machine {machine}")
            print(f"file path: {fileName}")
            with open(fileName) as f:
                dnaList = f.readlines()
                editionCount = int(dnaList[0]) # starting place for image names

                for x in range(1, len(dnaList)):
                    dna = getDnaFromStr(dnaList[x])
                    imageFiles = []
                    if (DEBUG):
                        print(f"split dna: {dna}")

                    for i in range(len(traits)):
                        selectedAttr: Attribute = traits[i].attributes[dna[i]]

                        if (DEBUG):
                            print(f"Trait: {traits[i].name}, Attribute: {selectedAttr.label}")
                        
                        if selectedAttr.label != NONE:
                            imageFiles.append(dir + PROJ_DIR + selectedAttr.file)


                    # save image to output file
                    saveImage(imageFiles, editionCount, dir)

                    editionCount += 1
        
        else:
            print(f"file not found, skipping for this machine: {fileName}")
        
generate()
