from distutils.debug import DEBUG
from typing import Collection, List
import random
import os
import json
from unicodedata import decimal
import csv
from dataclasses import dataclass
from enum import Enum

# Constants
NONE = "None"
DEBUG = True
IMAGE_URI = "metadata/"
PROJ_DIR = "/PBS_EX/"
FILE = "PBS_TST.csv"
EDITION_SIZE = 10
MACHINE_COUNT = 1
PROB_TOL = 0.001
GROUP_SIZE = int(EDITION_SIZE / MACHINE_COUNT)
DNA_FILE = "dna_group"
full_path = os.path.realpath(__file__)
path, filename = os.path.split(full_path)



class Attribute:
    def __init__(self, file, label, weight):
        self.file = file
        self.label = label
        self.weight = weight

class Trait:
    def __init__(self, attributes, name):
        self.attributes = attributes
        self.name = name
        
    def selectAttribute(self, randNum):
        sum = 0
        weights = []
        for attribute in self.attributes:
            weights.append(attribute.weight + sum)
            sum += attribute.weight

        if abs(sum - 1) >= PROB_TOL:
            print(f"Error! Sum of weights for trait {self.name} was {sum}, expected 1")
            return "", -1

        for i in range (0, len(self.attributes)):
            if weights[i] >= randNum:
                return self.attributes[i], i
        return "", -1



def getTraitsFromFile():
    # file needs following column structure to be read correctly:
    # trait, attribute, image file name, probability from 0-1, layer order from 0 (base) to layer count - 1(top layer)
    with open(path + PROJ_DIR + FILE) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        # next(reader, None)  # skip the headers
        traits = []

        isFirstRow = True
        for row in reader:
            if isFirstRow:
                isFirstRow = False
                continue

            # exit if we hit end of trait list
            if row[0] == '':
                break
            traitName = row[0]
            label = row[1]
            fileName = row[2]
            probability = float(row[3])

            newAttribute = Attribute(fileName, label, probability)

            if DEBUG:
                print(f"Trait: {traitName}, Attribute: {label}, File: {fileName}, Prob: {probability}")

            traitExists = False
            for trait in traits:
                if trait.name == traitName:
                    trait.attributes.append(newAttribute)
                    traitExists = True
                    break

            if not traitExists:
                traits.append(Trait([newAttribute], traitName))

    return traits