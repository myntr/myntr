import random
import os
import json
from dataclasses import dataclass
from enum import Enum
from config import *
from config import Trait, Attribute, getTraitsFromFile


def selectTrait(trait):
    randNum = random.uniform(0, 1)
    return trait.selectAttribute(randNum)


# Metadata Functions
def addMetaData(edition, attrList):
    metaData = {
        "name": f"Item #{edition}",
        "image": f"{IMAGE_URI}/{edition}.png",
        "attributes": attrList
    }
    metaDataList.append(metaData)


def saveMetaData(data, dir):
    with open(dir, 'w') as f:
        json.dump(data, f)
    

def saveMetaDataSingleFile(edition, dir):
    with open(dir, 'w') as f:
        json.dump(metaDataList[edition - 1], f)

def convertToStr(attrIndex):
    if attrIndex >= 10:
        return str(attrIndex)
    
    return '0' + str(attrIndex)


metaDataList = []
def generateDNA():
    
    print("\nstarting NFT DNA generation...")
    dnaList = []
    editionCount = 1
    dir = os.path.dirname(os.path.realpath(__file__))
    timeout = 0
    timeoutMax = 5
    traits = getTraitsFromFile()
    while editionCount <= EDITION_SIZE:
        if timeout >= timeoutMax:
            print("timeout reached, killing program")
            break
        
        dna = ""
        attrList = []
        for trait in traits:
            selectedAttr, attrIndex = selectTrait(trait)
            
                
            attrList.append({
                "trait_type": trait.name,
                "value": selectedAttr.label
            })


            dna += f"{convertToStr(attrIndex)}"
            
        if dna in dnaList:
            print(f"dna {dna} already exists, skipping")
            timeout += 1
            continue


        dnaList.append(dna)
        addMetaData(editionCount, attrList)
        saveMetaDataSingleFile(editionCount, dir + f"{PROJ_DIR}output/{editionCount}.json")
        
        
        editionCount += 1
    
    saveMetaData(metaDataList, dir + f"{PROJ_DIR}output/metadata.json")

    # write data to machine dna files
    editionCount = 1
    for machine in range(1, MACHINE_COUNT + 1):
        with open( dir + PROJ_DIR + DNA_FILE + str(machine) + '.txt', 'w') as f:
            # give start index to machine
            f.write(str(editionCount))
            f.write('\n')

            for i in range(GROUP_SIZE):
                f.write(dnaList[i])
                f.write('\n')
                editionCount += 1


        
generateDNA()
